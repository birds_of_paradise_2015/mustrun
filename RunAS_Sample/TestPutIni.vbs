Option Explicit
' coding: shift_jis
'
' This file is a part of MustRun Library.
'
' Copyright (c) 2010, 2015 極楽鳥(bird_of_paradise)
'
' This software is released under the MIT License.
' http://opensource.org/licenses/mit-license.php
'
' ##############################################################################
'
' AcadRemocon.dll の PutIni/GetiniStr メソッドの書き込み／読み込みテストを
' 実行します。
' Windows 8 以降では、権限の昇格が必要です。


Set Acad = CreateObject("AcadRemocon.Body")

Dim TestString_A, TestString_B

TestString_A = InputBox("AcadRemocon.iniファイルへの書き込みテストを行います。" & vbCRLF & vbCRLF & _
                        "何か文字列を入力してください。", _
                        "AcadRemocon.ini 書き込みテスト", "（値なし）")

If TestString_A = "" Then
    MsgBox "文字列を入力してください。", vbOKOnly, "AcadRemocon.ini 書き込み／読み込みテスト"
    WScript.Quit
End If

Acad.PutIni    TestString_A, "TEST_STRING", "MUSTRUN_TEST_SECTION"
Acad.GetIniStr TestString_B, "TEST_STRING", "MUSTRUN_TEST_SECTION"

Dim Result_String
If TestString_A = TestString_B Then
    Result_String = "文字列は合っています。"
Else
    Result_String = "文字列は合っていません。" & vbCRLF & vbCRLF & _
                    "このVBScriptファイルを「管理者として実行」してください。"
End If

Result = MsgBox("書き込んだ文字列は「" & TestString_A & "」です。" & vbCRLF & vbCRLF & _
                "読み込んだ文字列は「" & TestString_B & "」です。" & vbCRLF & vbCRLF & _
                Result_String, _
                vbOKOnly, "AcadRemocon.ini 読み込みテスト")
