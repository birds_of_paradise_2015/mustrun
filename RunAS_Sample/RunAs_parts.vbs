Option Explicit
' coding: shift_jis

' ##############################################################################

Public Sub RunAs()
    ' Sub RunAs()
    '
    ' This Sub Procedure is a part of MustRun Library.
    '
    ' 権限の昇格を実行します。
    ' 「RUNAS_MODE」を引数として、スクリプトを再度実行します。
    ' 引数を取るようなスクリプトの場合、このSubは適宜修正が必要です。

    Dim objFso    ' Scripting.FileSystemObject オブジェクト
    Dim strWSHost ' WScript.exe or CScript.exe

    Set objFso = WScript.CreateObject("Scripting.FileSystemObject")
    strWSHost = objFso.GetFileName(WScript.FullName)

    do while WScript.Arguments.Count = 0 and WScript.Version >= 5.7
        CreateObject("Shell.Application").ShellExecute strWSHost, _
                     """" & WScript.ScriptFullName & """" & " RUNAS_MODE", _
                     "", _
                     "runas", 1
        WScript.Quit
    loop

End Sub
