Option Explicit
' coding: shift_jis
'
' ar_env_checker.vbs
'
' システム変数「PROCESSOR_ARCHITECTURE」、「PROCESSOR_ARCHITEW6432」の
' 値を調べる。（OSは32 bitなのか、64 bitなのか？）
' AcadRemocon.dll、BcadRemocon.dllが利用可能かどうか、確認する。
'
' A part of MustRun Library.
'
' ##############################################################################
'
' The MIT License (MIT)
'
' Copyright (c) 2010, 2015 極楽鳥(bird_of_paradise)
'
' Permission is hereby granted, free of charge, to any person obtaining a copy
' of this software and associated documentation files (the "Software"), to deal
' in the Software without restriction, including without limitation the rights
' to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
' copies of the Software, and to permit persons to whom the Software is
' furnished to do so, subject to the following conditions:
'
' The above copyright notice and this permission notice shall be included in all
' copies or substantial portions of the Software.
'
' THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
' IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
' FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
' AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
' LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
' OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
' SOFTWARE.
'
' ##############################################################################
'
' win7 に AutoCAD 2006 (ADT 2006)【AutoCAD 対処法 ブログ】
' http://kadoban2design.seesaa.net/article/156783047.html
'
' How to Install .net framework 1.1 in Windows 7 64-bit? - Microsoft Answers
' http://answers.microsoft.com/en-us/windows/forum/windows_7-windows_programs/how-to-install-net-framework-11-in-windows-7-64/eb1e6232-e874-432e-ab43-17660e25e43d
'
' WOW64 実装の詳細
' http://msdn.microsoft.com/ja-jp/library/aa384274(v=vs.85).aspx
'
' ##############################################################################
'

If MustRun Then WScript.Quit '二重起動しているか？

Dim MyResult, objWshShell

MyResult = ""

Set objWshShell = WScript.CreateObject("WScript.Shell")

MyResult = MyResult & "PROCESSOR_ARCHITECTURE: " & _
           objWshShell.ExpandEnvironmentStrings("%PROCESSOR_ARCHITECTURE%") & _
           vbCRLF

Dim MyProcessorArchiteW6432
MyProcessorArchiteW6432 = objWshShell.ExpandEnvironmentStrings("%PROCESSOR_ARCHITEW6432%")
If MyProcessorArchiteW6432 <> "%PROCESSOR_ARCHITEW6432%" Then
    MyResult = MyResult & "PROCESSOR_ARCHITEW6432: " & _
               MyProcessorArchiteW6432 & vbCRLF
Else
    MyResult = MyResult & "PROCESSOR_ARCHITEW6432: No Variable" & vbCRLF
End If

' AcadRemocon.dll、BcadRemocon.dllが利用可能かどうか、確認する。
MyResult = MyResult & DllStatus("AcadRemocon.Body", "AcadRemocon.dll")
MyResult = MyResult & DllStatus("BcadRemocon.Body", "BcadRemocon.dll")

' クリップボードへ結果の文字列をコピーする。
SetClipboardData MyResult

' 結果表示
WScript.Echo MyResult

WScript.Quit


Function DllStatus(strProgID, strDllName)
    ' Comオブジェクトが利用可能かどうか、確認する。
    ' CreateObjectに成功すれば、「OK」
    ' 失敗すれば、「NG」
    Dim ComObj

    On Error Resume Next
    Set ComObj = CreateObject(strProgID)
    If Err.Number = 0 Then
        DllStatus = strDllName & ": OK" & vbCRLF
    Else
        DllStatus = strDllName & ": NG" & vbCRLF & _
                    "    Error: " & Err.Description & vbCRLF
        Err.Clear
    End If
    On Error GoTo 0

    Set ComObj = Nothing

End Function

Sub SetClipboardData(strData)
    ' クリップボードへ文字列をコピーする。
    '
    ' 以下のコードを実行するにはIEの設定をする必要がある。
    ' IE9の場合；
    ' IEを起動して「インターネットオプション」→ 「セキュリティ」タブ→
    ' 「レベルのカスタマイズ」ボタンをクリック
    ' 「スクリプト」項目から「スクリプトによる貼り付け処理の許可」の設定を
    ' 有効にする。
    Dim IE
    Set IE = CreateObject("InternetExplorer.Application")
    IE.Visible = false
    IE.Navigate "about:blank"

    Do Until IE.ReadyState = 4
        WScript.Sleep 10
    Loop

    IE.Document.ParentWindow.ClipboardData.SetData "Text", strData
    IE.Quit

    Set IE = Nothing

End Sub

Public Function MustRun()
    ' Function MustRun()
    '
    ' This function is a part of MustRun Library.
    '
    ' Copyright (c) 2010, 2015 極楽鳥(bird_of_paradise)
    '
    ' This software is released under the MIT License.
    ' http://opensource.org/licenses/mit-license.php
    '
    ' 実行している環境が64ビット環境か、32ビット環境か判断し、
    ' 64ビット環境であれば、32ビット環境で自身の再起動を試みる。
    ' そして、Trueを返す。32ビット環境であれば、Falseを返す。

    Dim objFso                   ' Scripting.FileSystemObject オブジェクト
    Dim objWshShell              ' WScript.Shell オブジェクト
    Dim strWSHost                ' WScript.exe or CScript.exe
    Dim strProcessorArchitecture ' CPUの種類
    Dim strExecCmd               ' 実行するコマンドライン

    Set objFso = WScript.CreateObject("Scripting.FileSystemObject")
    strWSHost = objFso.GetFileName(WScript.FullName)

    Set objWshShell = WScript.CreateObject("WScript.Shell")
    strProcessorArchitecture = objWshShell.ExpandEnvironmentStrings("%PROCESSOR_ARCHITECTURE%")
    If strProcessorArchitecture = "AMD64" Or strProcessorArchitecture = "IA64" Then
        strExecCmd = """" & objWshShell.ExpandEnvironmentStrings("%WINDIR%") & _
                     "\SysWOW64\" & strWSHost & """" & " " & _
                     """" & WScript.ScriptFullName & """"
        ' WScript.Echo strExecCmd
        objWshShell.Exec(strExecCmd)
        MustRun = True
    Else ' x86
        MustRun = False
    End If

    Set objFso                   = Nothing
    Set objWshShell              = Nothing
    Set strWSHost                = Nothing
    Set strProcessorArchitecture = Nothing
    Set strExecCmd               = Nothing

End Function
