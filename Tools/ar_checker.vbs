Option Explicit
' coding: shift_jis
'
' ar_checker.vbs
'
' AcadRemocon.dllがレジストリ登録されているか、
' 或いは、利用可能かどうか、確認する。
' CreateObjectに成功すれば、「OK」
' 失敗すれば、「NG」
'
' A part of MustRun Library.
'
' ##############################################################################
'
' The MIT License (MIT)
'
' Copyright (c) 2010, 2015 極楽鳥(bird_of_paradise)
'
' Permission is hereby granted, free of charge, to any person obtaining a copy
' of this software and associated documentation files (the "Software"), to deal
' in the Software without restriction, including without limitation the rights
' to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
' copies of the Software, and to permit persons to whom the Software is
' furnished to do so, subject to the following conditions:
'
' The above copyright notice and this permission notice shall be included in all
' copies or substantial portions of the Software.
'
' THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
' IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
' FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
' AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
' LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
' OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
' SOFTWARE.
'
' ##############################################################################
'

If MustRun Then WScript.Quit '二重起動しているか？

On Error Resume Next

Dim Acad
Set Acad = CreateObject("AcadRemocon.Body")

If Err.Number = 0 Then
    WScript.Echo "OK"
Else
    WScript.Echo "NG"
'    WScript.Echo "エラー: " & Err.Description
End If

Public Function MustRun()
    ' Function MustRun()
    '
    ' This function is a part of MustRun Library.
    '
    ' Copyright (c) 2010, 2015 極楽鳥(bird_of_paradise)
    '
    ' This software is released under the MIT License.
    ' http://opensource.org/licenses/mit-license.php
    '
    ' 実行している環境が64ビット環境か、32ビット環境か判断し、
    ' 64ビット環境であれば、32ビット環境で自身の再起動を試みる。
    ' そして、Trueを返す。32ビット環境であれば、Falseを返す。

    Dim objFso                   ' Scripting.FileSystemObject オブジェクト
    Dim objWshShell              ' WScript.Shell オブジェクト
    Dim strWSHost                ' WScript.exe or CScript.exe
    Dim strProcessorArchitecture ' CPUの種類
    Dim strExecCmd               ' 実行するコマンドライン

    Set objFso = WScript.CreateObject("Scripting.FileSystemObject")
    strWSHost = objFso.GetFileName(WScript.FullName)

    Set objWshShell = WScript.CreateObject("WScript.Shell")
    strProcessorArchitecture = objWshShell.ExpandEnvironmentStrings("%PROCESSOR_ARCHITECTURE%")
    If strProcessorArchitecture = "AMD64" Or strProcessorArchitecture = "IA64" Then
        strExecCmd = """" & objWshShell.ExpandEnvironmentStrings("%WINDIR%") & _
                     "\SysWOW64\" & strWSHost & """" & " " & _
                     """" & WScript.ScriptFullName & """"
        ' WScript.Echo strExecCmd
        objWshShell.Exec(strExecCmd)
        MustRun = True
    Else ' x86
        MustRun = False
    End If

    Set objFso                   = Nothing
    Set objWshShell              = Nothing
    Set strWSHost                = Nothing
    Set strProcessorArchitecture = Nothing
    Set strExecCmd               = Nothing

End Function
