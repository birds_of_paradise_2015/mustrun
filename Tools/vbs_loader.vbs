Option Explicit
' coding: shift_jis
'
' 実行している環境が64ビット環境か、32ビット環境か判断し、
' 64ビット環境であれば、32ビット環境で引数で指定された
' VBScriptファイルを起動する。32ビット環境であれば、
' そのまま引数で指定されたVBScriptファイルを起動する。
'
' 使い方；
'
' 実行するVBScriptファイル：Sample.vbs
' 　　　　　　　オプション：/a /b
'
' であったとすると、
' コマンドプロンプトで以下のようにタイプし、実行する。
'
' vbs_loader.vbs Sample.vbs /a /b
'
' A part of MustRun Library.
'
' ##############################################################################
'
' The MIT License (MIT)
'
' Copyright (c) 2010, 2015 極楽鳥(bird_of_paradise)
'
' Permission is hereby granted, free of charge, to any person obtaining a copy
' of this software and associated documentation files (the "Software"), to deal
' in the Software without restriction, including without limitation the rights
' to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
' copies of the Software, and to permit persons to whom the Software is
' furnished to do so, subject to the following conditions:
'
' The above copyright notice and this permission notice shall be included in all
' copies or substantial portions of the Software.
'
' THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
' IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
' FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
' AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
' LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
' OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
' SOFTWARE.
'
' ##############################################################################

Dim objArgs    ' 引数オブジェクト
Dim strExecCmd ' 実行するコマンドライン
Dim i
Set objArgs = WScript.Arguments
strExecCmd = ""
' 引数を連結する。
' 引数に空白が含まれている場合は、単純に「"」で囲い直す。
For i = 0 to objArgs.Count - 1
   If InStr(objArgs(i), " ") <> 0 Then 
       strExecCmd = strExecCmd & """" & objArgs(i) & """" & " "
   Else
       strExecCmd = strExecCmd & objArgs(i) & " "
   End If
Next
strExecCmd = RTrim(strExecCmd)

Dim objFso    ' Scripting.FileSystemObject オブジェクト
Dim strWSHost ' WScript.exe or CScript.exe
Set objFso = WScript.CreateObject("Scripting.FileSystemObject")
strWSHost = objFso.GetFileName(WScript.FullName)

Dim objWshShell              ' WScript.Shell オブジェクト
Dim strProcessorArchitecture ' CPUの種類
' 64ビット環境かどうか判断する。
Set objWshShell = WScript.CreateObject("WScript.Shell")
strProcessorArchitecture = objWshShell.ExpandEnvironmentStrings("%PROCESSOR_ARCHITECTURE%")
If strProcessorArchitecture = "AMD64" Or strProcessorArchitecture = "IA64" Then
    strExecCmd = """" & objWshShell.ExpandEnvironmentStrings("%WINDIR%") & _
                 "\SysWOW64\" & strWSHost & """" & " " & _
                 strExecCmd
End If

' WScript.Echo "実行するコマンドライン: " & strExecCmd
objWshShell.Exec(strExecCmd)

Set objFso                   = Nothing
Set objWshShell              = Nothing
Set strWSHost                = Nothing
Set strProcessorArchitecture = Nothing
Set strExecCmd               = Nothing
