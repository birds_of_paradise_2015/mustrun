' coding: shift_jis
'
' This file is a part of MustRun Library.
'

'========== ***** ↓↓↓↓↓ ***** ==========
If MustRun Then WScript.Quit '二重起動しているか？
'========== ***** ↑↑↑↑↑ ***** ==========

Set Acad = CreateObject("AcadRemocon.Body")
MsgBox Acad.Version

' ##############################################################################

Public Function MustRun()
    ' Function MustRun()
    '
    ' This function is a part of MustRun Library.
    '
    ' Copyright (c) 2010, 2015 極楽鳥(bird_of_paradise)
    '
    ' This software is released under the MIT License.
    ' http://opensource.org/licenses/mit-license.php
    '
    ' 実行している環境が64ビット環境か、32ビット環境か判断し、
    ' 64ビット環境であれば、32ビット環境で自身の再起動を試みる。
    ' そして、Trueを返す。32ビット環境であれば、Falseを返す。

    Dim objFso                   ' Scripting.FileSystemObject オブジェクト
    Dim objWshShell              ' WScript.Shell オブジェクト
    Dim strWSHost                ' WScript.exe or CScript.exe
    Dim strProcessorArchitecture ' CPUの種類
    Dim strExecCmd               ' 実行するコマンドライン

    Set objFso = WScript.CreateObject("Scripting.FileSystemObject")
    strWSHost = objFso.GetFileName(WScript.FullName)

    Set objWshShell = WScript.CreateObject("WScript.Shell")
    strProcessorArchitecture = objWshShell.ExpandEnvironmentStrings("%PROCESSOR_ARCHITECTURE%")
    If strProcessorArchitecture = "AMD64" Or strProcessorArchitecture = "IA64" Then
        strExecCmd = """" & objWshShell.ExpandEnvironmentStrings("%WINDIR%") & _
                     "\SysWOW64\" & strWSHost & """" & " " & _
                     """" & WScript.ScriptFullName & """"
        ' WScript.Echo strExecCmd
        objWshShell.Exec(strExecCmd)
        MustRun = True
    Else ' x86
        MustRun = False
    End If

    Set objFso                   = Nothing
    Set objWshShell              = Nothing
    Set strWSHost                = Nothing
    Set strProcessorArchitecture = Nothing
    Set strExecCmd               = Nothing

End Function
