# MustRun

64 bit OS 環境下で AcadRemocon.dll を比較的容易に利用するためのライブラリ。

本ライブラリを利用しなくとも、AcadRemocon.dll は勿論動きます。

-----

## ファイル一覧

- MustRun_parts.vbs

    このファイルの内容を、使用するVBScriptファイル内に
    貼り付ける。
    或いは、テンプレートとして使用する。

### Tools フォルダー

- ar_checker.vbs

    AcadRemocon.dll が利用可能かどうか、確認する。

    CreateObjectに成功すれば「OK」、  失敗すれば「NG」

    失敗だと AcadRemocon.dll を手動でレジストリ登録する必要あり。

    AcadRemocon.msi か Aremoupd.exe を使用してインストール
    する場合、AcadRemocon.dll を手動でレジストリ登録する
    機会は恐らくない。

- ar_env_checker.vbs

    ar_checker.vbs の拡張版。

    - BcadRemocon.dll 対応
    - 結果のクリップボードへのコピー対応

- vbs_loader.vbs

    実行している環境が64ビット環境か、32ビット環境か判断し、
    64ビット環境であれば、32ビット環境で引数で指定された
    VBScriptファイルを起動する。32ビット環境であれば、
    そのまま引数で指定されたVBScriptファイルを起動する。

### Sample フォルダー

64ビット環境から起動するように修正しているVBScript
サンプルファイル。

- Sample_acGetVar.vbs
- Sample_DxfExtract.vbs
- Sample_Version.vbs

### RunAS_Sample フォルダー

「権限の昇格」サンプル用ファイル。

AcadRemocon.dll の PutIni/GetiniStr メソッドの
書き込み／読み込みテストを実行します。


- RunAs_parts.vbs
- TestPutIni.bat
- TestPutIni.vbs

#### RunAS_Sample\Image フォルダー

「権限の昇格」説明用画像。

- Icon.jpg
- RightClickMenu.jpg
- Property1.jpg
- Property2.jpg

## 動作確認環境

- Windows 7 Professional (64 bit)
- AutoCAD 2011 (64 bit)
- AcadRemocon.dll 3.4.0.3

## ライセンス

[the MIT License](http://opensource.org/licenses/mit-license.php)
で公開しています。

## 履歴

- 2010/09/26 acadremocon.netホーム 掲示板 にて公開
- 2015/12/18 改訂して再公開

## 自作リポジトリへのリンク

- [AcadRemocon.dllの「使用例」をIronPythonへ移植](https://bitbucket.org/birds_of_paradise_2015/acadremocon_and_ironpython_sample)
- [AcadRemocon.dllの「使用例」をIronRubyへ移植](https://bitbucket.org/birds_of_paradise_2015/acadremocon_and_ironruby_sample)
- [AutoCAD 遠隔操作用Pythonライブラリ（試作）](https://bitbucket.org/birds_of_paradise_2015/acadremoteoperator)
- [junk_script（ezdxfを利用したサンプルコードなど）](https://bitbucket.org/birds_of_paradise_2015/junk_script)

-----

## 「権限の昇格」

Windows 8 以降で AcadRemocon.dll の PutIni メソッドを
利用する場合、Windows フォルダーへの書き込みには
管理者権限が必要です。

VBScript ファイルにコードを書き加えて、対応することも可能ですが、
MustRun Function を含めると同じ VBScript ファイルを
都合三度起動することになるので、いささか煩雑にすぎます。

バッチファイルを作成して対処する方がスマートかと思います。
この場合、**MustRun Function を VBScript ファイル内に書き加える必要はありません。**

RunAS_Sample フォルダーにサンプルファイルを用意しています。

-----

バッチファイルを右クリックして、「管理者として実行(A)」を選択します。

![バッチファイルの右クリックメニュー](https://bitbucket.org/birds_of_paradise_2015/mustrun/raw/master/RunAS_Sample/Image/RightClickMenu.jpg)

常に、「管理者として実行」する場合は、作成したバッチファイルに
対して、更にショートカットを作成します。

![バッチファイルのショートカットアイコン](https://bitbucket.org/birds_of_paradise_2015/mustrun/raw/master/RunAS_Sample/Image/Icon.jpg)

作成したショートカットを右クリックしてプロパティを表示します。

「詳細設定(D)...」ボタンをクリックします。

![ショートカットのプロパティ](https://bitbucket.org/birds_of_paradise_2015/mustrun/raw/master/RunAS_Sample/Image/Property1.jpg)

「管理者として実行(R)」にチェックして、「OK」ボタンをクリックします。

プロパティも、「OK」ボタンをクリックします。

![詳細プロパティ](https://bitbucket.org/birds_of_paradise_2015/mustrun/raw/master/RunAS_Sample/Image/Property2.jpg)

これで、VBScript ファイルは常に「管理者として実行」
するようになります。
